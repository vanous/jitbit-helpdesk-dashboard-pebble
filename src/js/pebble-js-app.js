var total_mine = 0;
var total_new = 0;
var total_unanswered = 0;
var total_unanswered_mine = 0;
var total_unclosed = 0;
var total_unclosed_mine = 0;
var city, temperature, icon, newt;
var hd_user, hd_passwd, hd_url, hd_hash, hd_userid, hd_fullname;
city = 0;
temperature = 0;
icon = 0;
newt = 0;
//localStorage.clear();
//window.localStorage.clear();

//get stored data



var previous_total_new, previous_total_unanswered, prevous_total_unclosed, previous_total_mine;
var new_finished = false;
var pending_finished = false;




function submit(){
		icon=iconFromWeatherId(total_unanswered);
		//console.log("submit: icon: "+ icon + " new: " + newt + " temp: " + temperature + " city: " + city);
		var retries = 0;
	function send(){
		var transID = Pebble.sendAppMessage({
            "icon":icon,
            "newt":newt,
            "temperature":temperature,
            "city":city},
			function(_e) {
				console.log("Completed transaction #" + transID );
				},
			function(_e) {
				console.log("Couldn send "+_e.error.message + " " + transID);
				if (retries++ < 3) {
					console.log("retry " + retry + " transaction #" + transID);
					send();
					}
				}
			);
	};
	send();
};



function get_credentials(callback){
	hd_user= localStorage.getItem("hd_user");
	hd_passwd= localStorage.getItem("hd_passwd");
	hd_url= localStorage.getItem("hd_url");
	hd_userid = localStorage.getItem("hd_userid");
	//console.log(hd_user);

	if ((hd_user === undefined) || (hd_passwd === undefined) || (hd_url === undefined)) {
		hd_user="";
		hd_passwd="";
		hd_url="";
		}
	hd_hash=Base64.encode(hd_user + ":" + hd_passwd); 
	//console.log("config data: " + hd_user + " " + hd_passwd + " " + hd_hash +" "+ hd_url);
	
			//console.log("local user id " + hd_userid);
	if (hd_userid===null){
		//get usedid
		var request = new XMLHttpRequest();
		request.onreadystatechange=function() {
		  if (request.readyState==4 && request.status==200)
			{
			var resp=request.responseText;
			////console.log(resp);
			var respobj=JSON.parse(resp);
			//console.log(respobj);
			hd_userid=respobj["UserID"]

			localStorage.setItem("hd_userid",hd_userid);
			//console.log("online user id " + hd_userid);
			callback && callback();
			}
		  };

		request.open('POST', hd_url + "/api/Authorization", true);
	    request.setRequestHeader('Content-Type','application/json');
		request.setRequestHeader('Authorization', "Basic "+ hd_hash);

		request.send();
		}else{
			callback && callback();
		}

}

function get_new(){
	var request = new XMLHttpRequest();
	new_finished=false;

	request.onreadystatechange=function() {
	  if (request.readyState==4 && request.status==200)
		{
		//console.log(request.responseText);
		var resp=request.responseText;
		//console.log(resp);
		var obj= JSON.parse(resp);
		var local=0;
		local = Object.keys(obj).length;
		total_new += local;
		

		if (local === 10) {
			//console.log("get next " + total);	
			get_unanswered();	
		}else{
		new_finished=true;
		if (pending_finished){
		new_finished=false;
		//console.log("new finishing subtracting, pending finished is: " + pending_finished);
		total_unanswered -= total_new;

		}else{
		//console.log("new finishing NOT finished subtracting, pending finished is: " + pending_finished);
		}
		//console.log("total new " + total_new);
		newt = "N " + total_new;
		submit();
		}
		}
	  };


	//console.log("getting tickets");
	request.open('GET', hd_url + "/api/Tickets?mode=unanswered&statusid=1&offset=" + total_new, true);

	request.setRequestHeader('Authorization', "Basic "+ hd_hash);
	request.send();
}

function get_unanswered(){
	var request = new XMLHttpRequest();
	pending_finished=false;

	request.onreadystatechange=function() {
	  if (request.readyState==4 && request.status==200)
		{
		//console.log(request.responseText);
		var resp=request.responseText;
		//console.log(resp);
		var obj= JSON.parse(resp);
		var local=0;
		local = Object.keys(obj).length;
		total_unanswered += local;
		
			for (var key in obj){
				
				if (obj[key]["AssignedToUserID"] == hd_userid){
					total_unanswered_mine +=1;	
				}
			}

		if (local === 10) {
			//console.log("get next " + total);	
			get_unanswered();	
		}else{
		pending_finished =true;
		if (new_finished){
		pending_finished=false;

		//console.log("pending finishing subtracting, new finished is: " + new_finished);
		total_unanswered -= total_new;
		}else{
		//console.log("pending finishing NOT subtracting, new finished is: " + new_finished);
		}
		//console.log("total pending " + total_unanswered);
		//console.log("ttl pending mine " + total_unanswered_mine);
		city = "P " + total_unanswered + "/" + total_unanswered_mine;
		submit();
		}
		}
	  };


	//console.log("getting tickets");
	request.open('GET', hd_url + "/api/Tickets?mode=unanswered&offset=" + total_unanswered, true);

	request.setRequestHeader('Authorization', "Basic "+ hd_hash);
	request.send();
}

function get_unclosed(){
	var request = new XMLHttpRequest();

	request.onreadystatechange=function() {
	  if (request.readyState==4 && request.status==200)
		{
		//console.log(request.responseText);
		var resp=request.responseText;
		//console.log(resp);
		var obj= JSON.parse(resp);
		var local=0;
		local = Object.keys(obj).length;
		total_unclosed += local;
			for (var key in obj){
				
				if (obj[key]["AssignedToUserID"] == hd_userid){
					total_unclosed_mine +=1;	
				}
			}

		if (local === 10) {
			//console.log("get next " + total);	
			get_unclosed();	
		}else{


			//console.log("total unclosed " + total_unclosed);
			//console.log("ttl unclosed mine " + total_unclosed_mine);
			temperature = "U " + total_unclosed +"/"+ total_unclosed_mine;
			submit();
		
		}
		}
	  };


	//console.log("getting tickets");
	request.open('GET', hd_url + "/api/Tickets?mode=unclosed&offset=" + total_unclosed, true);

	request.setRequestHeader('Authorization', "Basic "+ hd_hash);
	request.send();
}


function iconFromWeatherId(weatherId) {
  if (weatherId < 3) {
  //console.log(0);
    return 0;
  } else if (10 > weatherId > 5) {
  //console.log(1);
    return 1;
  } else if (20 > weatherId > 10) {
  //console.log(2);
    return 2;
  } else if (weatherId > 20) {
  //console.log(3);
    return 3;
  }
}



Pebble.addEventListener("ready",
                        function(e) {
						  console.log("ready event");
                          //console.log("connect! " + e.ready);
						  previous_total_new=total_new;
						  previous_total_unanswered= total_unanswered;
						  prevous_total_unclosed= total_unclosed;
						  total_new = 0;
						  total_unanswered = 0;
						  total_unclosed = 0;
						  total_unclosed_mine=0;
						  total_unanswered_mine = 0;

						  get_credentials(function(){
							  get_new();
							  get_unclosed();
							  get_unanswered();
							}
						  );





                          //console.log(e.type);
						  //console.log("ready!");
                        });


Pebble.addEventListener("appmessage",
                        function(e) {
							console.log("message event");
							previous_total_new=total_new;
							previous_total_unanswered= total_unanswered;
							prevous_total_unclosed= total_unclosed;

							total_new = 0;
							total_unanswered = 0;
							total_unanswered_mine = 0;
							total_unclosed = 0;
						    total_unclosed_mine=0;

							if ((hd_user != undefined) || (hd_passwd !=  undefined) || (hd_url != undefined) || (hd_userid != undefined)) {
							  get_new();
							  get_unclosed();
							  get_unanswered();
							  };

						  //console.log(e.type);
                          //console.log(e.payload);
                          //console.log("message!");
                        });



Pebble.addEventListener("showConfiguration",
  function(e) {
    //Load the remote config page
	//console.log("config start");
	var url = "https://dl.dropboxusercontent.com/s/d18mstvvfiqsht7/index.html?v=1";
	for(var i = 0, x = localStorage.length; i < x; i++) {
		var key = localStorage.key(i);
		var val = localStorage.getItem(key);
		
		if (val != undefined){
			url += "&" + encodeURIComponent(key) + "=" + encodeURIComponent(val);
		}
	}
	//console.log(url);
	
	Pebble.openURL(url);
    //Pebble.openURL("http://10.42.0.1/pebble.html"); //todo → send current configuration
    //Pebble.openURL("https://dl.dropboxusercontent.com/s/c69hnw1h62bv5ck/pebble.html"); //todo → send current configuration
	//file:///home/vanous/bin/pebble/developer.getpebble.com/blog/2013/12/20/Pebble-Javascript-Tips-and-Tricks/index.html

  }
);

Pebble.addEventListener("webviewclosed", function(_event) {
	if(_event.response) {
		var values = JSON.parse(decodeURIComponent(_event.response));
		
		for(key in values) {
			localStorage.setItem(key, values[key]);
		}
		get_credentials();
		
	}
});


var Base64 = {
// private property
_keyStr : "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",

// public method for encoding
encode : function (input) {
    var output = "";
    var chr1, chr2, chr3, enc1, enc2, enc3, enc4;
    var i = 0;

    input = Base64._utf8_encode(input);

    while (i < input.length) {

        chr1 = input.charCodeAt(i++);
        chr2 = input.charCodeAt(i++);
        chr3 = input.charCodeAt(i++);

        enc1 = chr1 >> 2;
        enc2 = ((chr1 & 3) << 4) | (chr2 >> 4);
        enc3 = ((chr2 & 15) << 2) | (chr3 >> 6);
        enc4 = chr3 & 63;

        if (isNaN(chr2)) {
            enc3 = enc4 = 64;
        } else if (isNaN(chr3)) {
            enc4 = 64;
        }

        output = output +
        Base64._keyStr.charAt(enc1) + Base64._keyStr.charAt(enc2) +
        Base64._keyStr.charAt(enc3) + Base64._keyStr.charAt(enc4);

    }

    return output;
},

// private method for UTF-8 encoding
_utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

        var c = string.charCodeAt(n);

        if (c < 128) {
            utftext += String.fromCharCode(c);
        }
        else if((c > 127) && (c < 2048)) {
            utftext += String.fromCharCode((c >> 6) | 192);
            utftext += String.fromCharCode((c & 63) | 128);
        }
        else {
            utftext += String.fromCharCode((c >> 12) | 224);
            utftext += String.fromCharCode(((c >> 6) & 63) | 128);
            utftext += String.fromCharCode((c & 63) | 128);
        }

    }

    return utftext;
}
}
